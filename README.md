JMU CS345 (Fall 2015)

Course Project

[Team D](team.md)

- - -

##Project Structure

The project is set up as an Eclipse project with Maven enabled.
The directory structure adheres to the Maven convention for source folders.

* `src/main/java` : The java sources to be included in the application build.

* `src/test/java` : The java sources for unit tests.

* `src/test/resources` : static resources for the testing stage.

## Maven Builds

`pom.xml` defines the maven build configuration. Standard maven commands can be used by triggering the Eclipse run command on this file.

`dev-resources/gutensearch - maven.launch` is an eclipse launch configuration that cleans and then builds the project by default.

### Enabled Maven Plugins

* `junit` : runs the JUnit test suits during the maven test phase.

* `javadoc` : generates the javadocs for classes in `src/main` as part of the packaging phase.  
    * Private members are enabled in the output.
    * JDK8 defaults to a strict validation mode called doclint.  The project is configured to continue if this stage generates an error.

### Maven output

Maven outputs it's work files to: `target/`

Special directories are allocated for deliverables.  These directories are ignored by git.

The following files will be built during a `mvn build` cycle:

* `dist/*project-id*-*version*.jar` is the compiled class archive.
* `dist/*project-id*-*version*-sources.jar` is the source file archive.
* `javadocs/` is the location for the raw javadoc output.
* `dist/*project-id*-javadoc.jar` is the archived javadoc.

## Style Guidelines

Checkstyle is enabled on the project in Eclipse, and the `checkstyle.xml` is provided in the `dev-resources/style-tools/`, along with an eclipse formatter file.

The formatter file may need to be enabled manually inside Eclipse.