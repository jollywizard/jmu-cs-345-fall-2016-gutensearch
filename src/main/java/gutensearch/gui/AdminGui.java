package gutensearch.gui;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**
 * This class creates the Administrator GUI that can be used to add files to the main storage.
 * 
 * @author Matthew Dromazos
 * @version 1.0 10/16/15
 * 
 *          This work complies with the JMU honor code.
 *
 */
public class AdminGui extends JLabel
{

  public static final String ADDPANEL = "Card to add file";
  public static final String ADDSUCCESSFULPANEL = "File added card";
  public static final String ADDNOTSUCCESSFULPANEL = "File not added card";
  public static final int FRAME_WIDTH = 500;
  public static final int FRAME_HEIGHT = 125;
  public static final int LABEL_WIDTH = 100;
  public static final int LABEL_HEIGHT = 25;
  public static final int FIELD_WIDTH = 150;

  /**
   * The serial version of the class.
   */
  private static final long serialVersionUID = 1L;

  private JFrame frame;
  private JLabel locLabel, docLabel, addSuccesfulLabel, notAddedLabel;
  private Border border;
  private JPanel addFileCard, addSuccessfulCard, fileNotAddedCard, cards;
  private JButton fileButton, addButton, addAnotherButton, tryAgainButton;
  private JTextField fileTextField, docTitleField, errorTextField;
  private Box horizBox, horizBox2, horizBox3, vertBox, vertBox2, vertBox3;
  private JFileChooser fc;
  private File file;

  /**
   * This constructor calls the method to create all parts of the Gui.
   */
  public AdminGui()
  {
    createAndShowGUI();
  }

  /**
   * Create the GUI and show it. For thread safety, this method should be invoked from the
   * event-dispatching thread.
   */
  public void createAndShowGUI()
  {
    // instantiate the components
    addFileCard = new JPanel();
    addSuccessfulCard = new JPanel();
    cards = new JPanel(new CardLayout());
    fileTextField = new JTextField();
    fileButton = new JButton("File Picker");
    border = BorderFactory.createEtchedBorder();
    locLabel = new JLabel("File Location");
    docLabel = new JLabel("Document File");
    addButton = new JButton("Add File");
    docTitleField = new JTextField();
    frame = new JFrame("Administrator");
    vertBox = Box.createVerticalBox();
    horizBox = Box.createHorizontalBox();
    horizBox2 = Box.createHorizontalBox();
    horizBox3 = Box.createHorizontalBox();
    fc = new JFileChooser();

    // allign the components for card1
    horizBox.setAlignmentX(LEFT_ALIGNMENT);
    horizBox2.setAlignmentX(LEFT_ALIGNMENT);
    horizBox3.setAlignmentX(LEFT_ALIGNMENT);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
    locLabel.setMaximumSize(new Dimension(LABEL_WIDTH, LABEL_HEIGHT));
    docLabel.setMaximumSize(new Dimension(LABEL_WIDTH, LABEL_HEIGHT));
    fileTextField.setMaximumSize(new Dimension(FIELD_WIDTH, LABEL_HEIGHT));
    docTitleField.setMaximumSize(new Dimension(FIELD_WIDTH, LABEL_HEIGHT));

    // Add the components and border
    horizBox.add(locLabel);
    horizBox.add(fileTextField);
    horizBox.add(fileButton);
    horizBox2.add(docLabel);
    horizBox2.add(docTitleField);
    horizBox3.add(addButton);
    vertBox.add(horizBox);
    vertBox.add(horizBox2);
    vertBox.add(horizBox3);
    addFileCard.setLayout(new BoxLayout(addFileCard, BoxLayout.Y_AXIS));
    addFileCard.add(vertBox);
    addFileCard.setBorder(border);

    // Create panel2
    addSuccessfulCard.setLayout(new BoxLayout(addSuccessfulCard, BoxLayout.Y_AXIS));
    vertBox2 = Box.createVerticalBox();
    addSuccesfulLabel = new JLabel("The document has been added successfully");
    addAnotherButton = new JButton("Add Another");
    vertBox2.add(addSuccesfulLabel);
    vertBox2.add(addAnotherButton);
    addSuccessfulCard.add(vertBox2);

    // create the notAddedCard
    tryAgainButton = new JButton("Try Again");
    fileNotAddedCard = new JPanel();
    vertBox3 = Box.createVerticalBox();
    fileNotAddedCard.setLayout(new BoxLayout(fileNotAddedCard, BoxLayout.Y_AXIS));
    notAddedLabel = new JLabel("The file was not able to be added.");
    errorTextField = new JTextField();
    errorTextField.setEditable(false);
    vertBox3.add(notAddedLabel);
    vertBox3.add(errorTextField);
    vertBox3.add(tryAgainButton);
    fileNotAddedCard.add(vertBox3);

    // add panels to the cards
    cards.add(addFileCard, ADDPANEL);
    cards.add(addSuccessfulCard, ADDSUCCESSFULPANEL);
    cards.add(fileNotAddedCard, ADDNOTSUCCESSFULPANEL);
    frame.add(cards);

    // add actionListeners
    addButton.addActionListener(new PanelSwitch());
    addAnotherButton.addActionListener(new PanelSwitch());
    fileButton.addActionListener(new PanelSwitch());
    tryAgainButton.addActionListener(new PanelSwitch());

    // display the frame
    display();
  }

  /**
   * This method packs the frame and makes it visible.
   */
  public void display()
  {
    frame.pack();
    frame.setVisible(true);
  }

  /**
   * This is the main method in which the program runs.
   * 
   * @param args
   *          from the command line.
   */
  public static void main(String args[])
  {
    new AdminGui();
  }

  /**
   * This method will be changed in later sprints to determine if the file chosen was correct and
   * added successfully.
   * 
   * @param f
   *          to be checked if added.
   * @return if the file was added.
   */
  public boolean addSuccessfull(File f)
  {
    return true;
  }

  /**
   * This class serves to switch the panels from the main panel, the panel if the file was added,
   * and the panel for if the file was not added successfully.
   * 
   * @author Matthew Dromazos
   * @version 1.0 10/26/15
   * 
   *          This work complies with the JMU honor code.
   *
   */
  public class PanelSwitch implements ActionListener
  {
    /**
     * This method is called whenever a button is pressed and switches the panels accordingly.
     * 
     * @param event
     *          the Action Event.
     */
    public void actionPerformed(ActionEvent event)
    {
      CardLayout cl = (CardLayout) (cards.getLayout());
      if (event.getSource() == addButton)
      {
        if (addSuccessfull(file))
        {
          cl.show(cards, ADDSUCCESSFULPANEL);
        }
        else
        {
          cl.show(cards, ADDNOTSUCCESSFULPANEL);
        }
      }
      else if (event.getSource() == addAnotherButton)
      {
        cl.show(cards, ADDPANEL);
      }
      else if (event.getSource() == fileButton)
      {
        int returnVal = fc.showOpenDialog(AdminGui.this);
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
          file = fc.getSelectedFile();
          fileTextField.setText(file.getAbsolutePath());
          docTitleField.setText(file.getName());
        }
      }
      else if (event.getSource() == tryAgainButton)
      {
        cl.show(cards, ADDPANEL);
      }
    }
  }
}

