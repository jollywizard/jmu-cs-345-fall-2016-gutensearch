package gutensearch.gui;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JList;


/**
 * This class holds the code for the UserGui interface.
 * 
 * @author Matthew Dromazos
 * @version 1.0 10/29/15
 * 
 * This work complies with the JMU honor code.
 *
 */
public class UserGui
{
  public static final String MAIN_USER_PANEL = "This is the main user gui.";
  public static final String CURRENT_QUERY_PANEL = "This panel shows the current query.";
  public static final int FRAME_WIDTH = 500;
  public static final int FRAME_HEIGHT = 550;
  
  private JFrame frame;
  private JLabel query, currentQuery, currentResult, queryTitle, docTitle;
  private JTextField docField, queryTextField;
  private JButton submitButton, clearButton;
  private JScrollPane queryPane;
  private JPanel addQueryCard, queryCard, cards;
  private Box horizAddBox, queryVertBox, horizTitleBox, horizDocBox;
  private Border border;
  private JList<String> paragList;
  private DefaultListModel<String> model;
  private CardLayout cl;
  private String[] paragraphs = {"Element 1", "Element 2", "Element 3"};
  
  /**
   * This constructor simply calls the createAndShowGui to make the
   * interface.
   */
  public UserGui()
  {
    createAndShowGui();
  }
  
  /**
   * This method creates the Gui and displays it.
   */
  public void createAndShowGui()
  { 
    //create the frame;
    frame = new JFrame("User Gui");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
    
    //create the addQueryCard
    query = new JLabel("Query");
    query.setPreferredSize(new Dimension(50, 25));
    queryTextField = new JTextField();
    queryTextField.setPreferredSize(new Dimension(250, 25));
    submitButton = new JButton("Submit");
    horizAddBox = Box.createHorizontalBox();
    addQueryCard = new JPanel();
    addQueryCard.setPreferredSize(new Dimension(FRAME_WIDTH, 100));
    border = BorderFactory.createEtchedBorder();
    addQueryCard.setBorder(border);
    horizAddBox.add(query);
    horizAddBox.add(queryTextField);
    addQueryCard.add(horizAddBox);
    addQueryCard.add(submitButton);
    
    //create the queryCard
    model = new DefaultListModel<String>();
    paragList = new JList<String>(model);
    addParagraphs(paragraphs);
    currentQuery = new JLabel("Current Query: ");
    queryTitle = new JLabel("");
    clearButton = new JButton("Clear Query");
    queryPane = new JScrollPane(paragList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS
            , JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    queryPane.setPreferredSize(new Dimension(400, 300));
    horizTitleBox = Box.createHorizontalBox();
    queryVertBox = Box.createVerticalBox();
    horizTitleBox.add(currentQuery);
    horizTitleBox.add(queryTitle);
    horizTitleBox.add(clearButton);
    queryVertBox.add(horizTitleBox);
    queryVertBox.add(queryPane);
    horizDocBox = Box.createHorizontalBox();
    currentResult = new JLabel("Current Result: ");
    docTitle = new JLabel("Paragraph Title");
    horizDocBox.add(currentResult);
    horizDocBox.add(docTitle);
    queryVertBox.add(horizDocBox);
    docField = new JTextField();
    docField.setPreferredSize(new Dimension(400, 175));
    queryVertBox.add(docField);
    queryCard = new JPanel();
    queryCard.add(queryVertBox);
    queryCard.setPreferredSize(new Dimension(500, 550));
    
    //create and cards
    cards = new JPanel(new CardLayout());
    cards.add(addQueryCard, MAIN_USER_PANEL);
    cards.add(queryCard, CURRENT_QUERY_PANEL);
    frame.add(cards);
    
    //add action listeners
    submitButton.addActionListener(new PanelSwitch());
    clearButton.addActionListener(new PanelSwitch());
    paragList.addListSelectionListener(new ListSelection());
    
    //call the display method
    display();
    
    cl  = (CardLayout)(cards.getLayout()); 
  }
  
  /**
   * This method is called to add paragraphs to the paragraph list.
   * 
   * @param ps : Paragraph text to add to the list.
   */
  public void addParagraphs(String[] ps)
  {
    for(String paragraph : ps)
    {
      model.addElement(paragraph);
    }
  }
  
  
  /**
   * This method packs and makes the Gui visible.
   */
  public void display()
  {
    frame.pack();
    frame.setVisible(true);
  }
  
  /**
   * This is the main method for which the program runs.
   * 
   * @param args Not used
   */
  public static void main(String args[])
  {
    new UserGui();
  }
  
  /**
   * 
   * @author Matthew Dromazos
   * @version 1.0 10/28/15
   * 
   * This work complies with the JMU honor code.
   *
   */
  public class PanelSwitch implements ActionListener
  {
    /**
     * This method is called whenever a button is pressed and
     * it changes the panel accordingly.
     * 
     * @param event ActionEvent when a button is pressed.
     */
    public void actionPerformed(ActionEvent event)
    {
      if (event.getSource() == submitButton)
      {
        queryTitle.setText(queryTextField.getText());
        cl.show(cards, CURRENT_QUERY_PANEL);
      }
      else if (event.getSource() == clearButton)
      {
        queryTitle.setText("");
        queryTextField.setText("");
        cl.show(cards, MAIN_USER_PANEL);
      }
    }
  }
  
  /**
   * This class holds the method for when a object in the list is selected.
   * 
   * @author Matthew Dromazos
   *
   */
  public class ListSelection implements ListSelectionListener
  {
    /**
     * this method is called when a list selection event occurs.
     * 
     * @param e : ListSelectionEvent when a list is selected.
     */
    public void valueChanged(ListSelectionEvent e)
    {
      docField.setText(paragList.getSelectedValue());
    }
    
  }
}

