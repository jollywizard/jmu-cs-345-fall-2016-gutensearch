package gutensearch.terms;

import java.util.Iterator;
import java.util.List;

/**
 * Abstract base class for {@link TermMapper} implementations.
 * <p>
 * Subclasses must implement {@link #tokenizeText(String)}, which splits the paragraph into terms.
 * <p>
 * If the {@link #filter} property is set, it can be used to approve/deny which terms enter the
 * index.
 * <p>
 * Resolution of equality between terms is abstracted with the {@link TermNormalizer} interface. If
 * the {@link #normalizer} property is set, all parameter terms will be converted to their canonical
 * representation via {@link #normalize(String)}, otherwise, the original term will be used.
 * 
 * @author James Arlow
 *
 */
public abstract class AbstractTermMapper implements TermMapper
{

  protected TermFilter filter;

  protected TermNormalizer normalizer;

  @Override
  public TermMap mapTermFrequencies(String text)
  {
    List<String> tokens = tokenizeText(text);
    Iterator<String> iter = tokens.iterator();
    if (filter != null)
      while (iter.hasNext())
      {
        if (!filter.useTerm(iter.next()))
          iter.remove();
      }

    TermMap tmap = new TermMap();
    for (String term : tokens)
    {
      tmap.add(normalize(term));
    }

    return tmap;
  }

  /**
   * Normalizes the term to it's canonical representation in this map.
   * <p>
   * If this map has a {@link TermNormalizer} property, the mapped/stored term may be different from
   * the input term. This method is used internally to normalize the term, and can be used
   * externally to ensure that analysis of the map is consistent and predictable.
   * <p>
   * 
   * @param term
   *          The term to normalize.
   * @return The normalized term, or the original term if no {@link TermNormalizer} is set.
   */
  public String normalize(String term)
  {
    return normalizer == null ? term : normalizer.normalizeTerm(term);
  }

  /**
   * Sets the normalizer that will be used to convert strings to their predicatable/searchable form.
   * 
   * @param normalizer
   *          The TermNormalizer to activate on this {@link TermMap}
   */
  public void setNormalizer(TermNormalizer normalizer)
  {
    this.normalizer = normalizer;
  }

  /**
   * Tokenizes the input stream. Subclasses of {@link AbstractTermMapper} must provide this
   * function.
   * 
   * @param text
   *          The text to tokenize (split into terms).
   * @return The List of tokenized terms.
   */
  protected abstract List<String> tokenizeText(String text);

}
