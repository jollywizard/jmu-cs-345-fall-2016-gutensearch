package gutensearch.terms;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * The default implementation of {@link TermMapper}.
 * <p>
 * <ul>
 * <li>Maps all terms as lowercase.
 * <li>Punctuation surrounded by word characters (alpha numeric) are allowed. This allows
 * underscores, apostrophes, dashes, etc, as long as they are inside a word.
 * </ul>
 * 
 * @author James Arlow
 *
 */
public class DefaultTermMapper extends AbstractTermMapper
{

  @Override
  protected List<String> tokenizeText(String text)
  {
    List<String> r = new LinkedList<String>();

    /*
     * This delimiter pattern skips punctuation not immediately followed by word characters (letters
     * and numbers), followed by spaces or end of input. The punctuation is optional.
     * 
     * The punctuation policy allows abbreviations and hyphenations to be valid terms.
     */
    final String delims = "(?:\\p{Punct}(?!\\w))*(\\p{Space}+|\\z)";

    Scanner scan = new Scanner(text);
    scan.useDelimiter(delims);
    while (scan.hasNext())
    {
      String term = scan.next();
      r.add(term.toLowerCase());
    }
    scan.close();
    return r;
  }

}
