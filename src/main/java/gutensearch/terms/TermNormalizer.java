package gutensearch.terms;

/**
 * 
 * A filter which establishes the canonical form for variants of a term.
 * 
 * @author James Arlow
 *
 */
public interface TermNormalizer
{
  /**
   * Convert the input term to it's canonical form.
   * 
   * @param term
   *          The term to normalize.
   * @return The canonical form of the term.
   */
  public String normalizeTerm(String term);
}
