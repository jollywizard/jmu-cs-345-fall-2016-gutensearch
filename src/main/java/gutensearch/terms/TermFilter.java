package gutensearch.terms;

/**
 * An interface which can be used as a filter to cherry pick terms.
 * 
 * @author James Arlow
 *
 */
public interface TermFilter
{

  /**
   * Approve or deny the term according to the implementation criteria.
   * 
   * @param term
   *          The term to approve or deny.
   * @return a boolean indicating the approval status of the term.
   */
  boolean useTerm(String term);

}
