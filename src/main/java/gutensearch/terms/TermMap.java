package gutensearch.terms;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * A term frequency map.
 * <p>
 * GutenSearch IRS data is analyzed for term frequency. This class provides a container for working
 * with this data. Internally, the data is stored as a map of Strings to Integers. A subset of
 * collection operations for working with this map in a consistent fashion are provided, as well as
 * a method to create a copy of the internal data as a standard Java {@link Map}
 * <p>
 * 
 * @author James Arlow
 */
public class TermMap implements Iterable<Entry<String, Integer>>, Serializable
{

  /**
   * Default Serialization ID.
   * <p>
   * Used to manage saved data compatibility between class versions.
   */
  private static final long serialVersionUID = 1L;

  protected Map<String, Integer> data = new TreeMap<>();

  /**
   * Increment the term count.
   * 
   * @param term
   *          The term count to increment.
   */
  public void add(String term)
  {
    Integer count = data.get(term);
    if (count == null)
      count = 1;
    else
      count++;
    data.put(term, count);
  }

  /**
   * Add a compatible map of terms to frequency data to this {@link TermMap}.
   * 
   * @param map
   *          The map data to add to this {@link TermMap}.
   */
  public void addAll(Map<String, Integer> map)
  {
    for (Entry<String, Integer> entry : map.entrySet())
    {
      set(entry.getKey(), entry.getValue());
    }
  }

  /**
   * True if the map contains the term.
   * 
   * @param term
   *          The term to test.
   * @return true if this map contains a positive count for the term.
   */
  public boolean contains(String term)
  {
    return data.containsKey(term) && data.get(term) > 0;
  }

  /**
   * Gets the frequency count for this term.
   * 
   * @param term
   *          The term to get the mapped count for.
   * @return 0 if the term is not mapped, else the mapped count for the term.
   */
  public int getFrequency(String term)
  {
    Integer r = data.get(term);
    return r == null ? 0 : r;
  }

  /**
   * An {@link Iterable} terms mapped by this collection.
   * 
   * @return An {@link Iterable} terms mapped by this collection.
   */
  public Iterable<String> getTerms()
  {
    return data.keySet();
  }

  @Override
  public Iterator<Entry<String, Integer>> iterator()
  {
    return data.entrySet().iterator();
  }

  /**
   * Updates the frequency count for a specific term.
   * 
   * @param term
   *          The term to update the count for.
   * @param count
   *          The frequency count to apply to the term.
   */
  public void set(String term, Integer count)
  {
    if (count == null || count == 0)
      data.remove(term);
    else
      data.put(term, count);
  }

  /**
   * Convert this {@link TermMap} to a Java {@link Map} of Strings to Integers.
   * 
   * @return this TermMap as a map of Strings to Integers.
   */
  public Map<String, Integer> toMap()
  {
    return new TreeMap<String, Integer>(data);
  }

  /**
   * Views this TermMap as Java Map using the toString of it's internal map data.
   * 
   * @return the Java {@link Map#toString()} representation of the TermMap data.
   */
  public String toString()
  {
    return data.toString();
  }

}
