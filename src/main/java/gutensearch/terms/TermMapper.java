package gutensearch.terms;

/**
 * Generates term frequency maps for from {@link Paragraph}.
 * 
 * @author James Arlow
 *
 */
public interface TermMapper
{

  /**
   * Generate a map of term frequencies from input text.
   * 
   * @param text
   *          The parameter to generate the term frequencies for.
   * @return A {@link TermMap} indexing the term frequencies of thi input text.
   */
  public TermMap mapTermFrequencies(String text);

}
