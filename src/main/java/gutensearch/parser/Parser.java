package gutensearch.parser;

import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gutensearch.datamodel.Document;
import gutensearch.datamodel.Paragraph;
import gutensearch.storage.FileAccess;
import gutensearch.storage.FileRef;

/**
 * This class constructs a {@link Document} descriptor from an input Project Gutenberg data file.
 * <p>
 * 
 * @author James Arlow
 */
public class Parser
{

  /**
   * Processing information is passed between stages using {@link Parser#metadata}. These are the
   * keys defined by this implementation for that collection.
   */
  static final class MetaKeys
  {
    static final String DOC_ID = "doc-key";
    static final String DOC_TITLE = "doc-title";
    static final String START_KEY = "start-marker";
    static final String STOP_KEY = "stop-marker";
  }

  /**
   * Regular Expression pattern strings for use during processing.
   */
  static final class Patterns
  {
    /**
     * Project Gutenberg files have project metadata on both sides of the document. The main
     * document is delimited on each end by marker lines. The output {@link Document} should not
     * include the delimiters or the exterior metadata.
     */
    static final String GUTEN_START = "" //Checkstlye requires this to be on a new line.
        + "\\*\\*\\* START OF THIS PROJECT GUTENBERG EBOOK (.*) \\*\\*\\*";

    /**
     * Project Gutenberg files have project metadata on both sides of the document. The main
     * document is delimited on each end by marker lines. The output {@link Document} should not
     * include the delimiters or the exterior metadata.
     */
    static final String GUTEN_STOP = "\\*\\*\\* END OF THIS PROJECT GUTENBERG EBOOK (.*) \\*\\*\\*";
  }

  /**
   * The output {@link Document} for this processor.
   */
  Document document;

  /**
   * Flexible container for passing parsing info between processing stages.
   */
  Properties metadata = new Properties();

  /**
   * The first stage of processing is to convert the input file into this list of paragraph
   * descriptors. The paragraphs are then filtered before being used to construct the output
   * {@link Document}.
   */
  List<Paragraph> paragraphs = new LinkedList<Paragraph>();

  /**
   * The location identifier of the data file to be processed.
   */
  FileRef target;

  /**
   * 
   * Creates a {@link Parser} that targets the parameter file.
   * 
   * @param target
   *          The {@link FileRef} describing the location details of the data file to be processed.
   */
  public Parser(FileRef target)
  {
    this.target = target;
  }

  /**
   * After all other processing phases have been completed, this method constructs the output
   * {@link Document} from the results.
   */
  private void buildDocument()
  {
    document = new Document();
    if (metadata.containsKey(MetaKeys.DOC_ID))
    {
      document.setKey(Integer.decode(metadata.getProperty(MetaKeys.DOC_ID)));
    }
    if (metadata.containsKey(MetaKeys.DOC_TITLE))
    {
      document.setTitle(metadata.getProperty(MetaKeys.DOC_TITLE));
    }
    document.setParagraphs(paragraphs);
  }

  /**
   * After the document has been separated into paragraphs, this method extracts metadata, and
   * removes paragraphs that are considered to be outside of the content area of the document.
   */
  private void filterParagraphs()
  {
    boolean prologue = true;
    boolean epilogue = false;
    int i = 0;
    Iterator<Paragraph> iter = paragraphs.iterator();
    while (iter.hasNext())
    {
      Paragraph p = iter.next();
      if (prologue)
      {
        Properties props = scanForMetadata(p);
        metadata.putAll(props);
        if (props.containsKey(MetaKeys.START_KEY))
        {
          prologue = false;
        }
        iter.remove();
      }
      else if (epilogue)
      {
        iter.remove();
      }
      else
      {
        if (scanForMetadata(p).containsKey(MetaKeys.STOP_KEY))
        {
          epilogue = true;
          iter.remove();
        }
        else
        {
          p.getId().setOrdinal(i++);
        }
      }
    }
  }

  /**
   * Gets the {@link Document} data model constructed by this Parser from the target.
   * <p>
   * The target is processed once. Subsequent calls will return the cached result.
   * 
   * @return The Document if it can be successfully extracted.
   * @throws Exception
   *           If an error occurs when processing the target file.
   */
  public Document getDocument() throws Exception
  {
    if (document == null)
      processTarget();
    return document;
  }

  /**
   * Initiates the construction of the output {@link Document}.
   * 
   * The target data file is read and converted into paragraphs, and then those paragraphs are
   * filtered and processed for metadata.
   * 
   * @throws Exception
   *           If any stage of the processing throws an Exception it is propagated.
   */
  public void processTarget() throws Exception
  {
    tokenizeParagraphs();
    filterParagraphs();
    buildDocument();
  }

  /**
   * During the {@link #filterParagraphs()} stage, paragraphs are fed here to be analyzed for
   * {@link Document} metadata.
   * <p>
   * Currently, it just scans for Project Gutenberg wrappers.
   * <p>
   * TODO: Convert this into a command pattern, so that it can be extended.
   * 
   * @param p
   *          The paragraph to scan for metadata
   * @return A {@link Properties} object that maps any metadata detected during
   */
  private Properties scanForMetadata(Paragraph p)
  {
    Properties props = new Properties();
    if (!metadata.containsKey(MetaKeys.START_KEY))
    {
      Matcher m = Pattern.compile(Patterns.GUTEN_START).matcher(p.getText());
      if (m.find())
      {
        props.put(MetaKeys.START_KEY, m.group());
        props.put(MetaKeys.DOC_TITLE, m.group(1));
      }
    }
    else
    {
      Matcher m = Pattern.compile(Patterns.GUTEN_STOP).matcher(p.getText());
      if (m.find())
      {
        props.put(MetaKeys.STOP_KEY, p.getText());
      }
    }

    return props;
  }

  /**
   * This processing stage uses the definition of paragraphs ( text separated by whitespace
   * consisting of two or more newlines) to tokenize the input document into a list of paragraph
   * text descriptors.
   * <p>
   * The raw data must be accessed to do this, so an I/O error can occur at the start of processing.
   * 
   * @throws Exception
   *           If an I/O error occurs when opening the document.
   */
  private void tokenizeParagraphs() throws Exception
  {
    InputStream is = FileAccess.openStream(target);
    Scanner s = new Scanner(is);

    String paragraphbreak = "\\s*[\n]{1,}" // whitespace at end of p1
        + "\\s*" // any amount of whitespace in between
        + "([\n]{1}\\s*|\\Z)" // p2 starts + indent || or EOF
        + "";
    s.useDelimiter(paragraphbreak);
    while (s.hasNext())
    {
      s.next();
      Paragraph p = new Paragraph();
      p.setText(s.match().group());
      paragraphs.add(p);
    }
    s.close();
  }

}
