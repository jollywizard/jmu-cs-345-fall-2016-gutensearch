
/**
 * Defines the gutensearch perspective of text documents.
 * <p>
 * {@link gutensearch.datamodel.Document} and {@link gutensearch.datamodel.Paragraph} describe the
 * structure of a Project Gutenberg ebook, and metadata needed to drive application functionality.
 * <p>
 * {@link gutensearch.datamodel.Index} provides a container which stores data model objects and
 * provides access to the document data and cross document term indexing.
 * <p>
 * Index is intended to be persisted between application instances.
 * 
 * @author James Arlow
 */
package gutensearch.datamodel;
