package gutensearch.datamodel;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

import gutensearch.terms.TermMap;
import gutensearch.terms.TermMapper;

/**
 * A collection of Documents and aggregate indexing of their Paragraph term frequencies.
 * <p>
 * Used to store the application wide collection of Documents and to provide basic term frequency
 * searching across all documents.
 * <h3>Information Retrieval Systems</h3>
 * <p>
 * In the language of an <b>Information Retrieval System</b>, {@link Index} is a container for the
 * IRS document store and the IRS inverted index.
 * <p>
 * The document store is represented by a list of guttensearch {@link Document}s, which store
 * ordered collections of IRS documents, represented by {@link Paragraph}.
 * <p>
 * The inverted index is represented by {@link TermIndex}, which maps terms to lists of
 * {@link Paragraph.Id} postings where they occur.
 * 
 * @author James Arlow
 *
 */
public class Index implements Serializable
{

  /**
   * Default Serialization ID.
   * <p>
   * Used to manage serialization compatibility
   */
  private static final long serialVersionUID = 1L;

  transient TermMapper termMapper;

  Documents documents = new Documents();

  TermIndex terms = new TermIndex();

  /**
   * {@link Document} objects added to this index that do not have an id set will have one assigned
   * from this counter, so that search functions can work properly.
   */
  private int idpool = 1;

  /**
   * Adds a Document to the index, ensuring that it has a key if it does not have one already.
   * 
   * @param doc
   *          The document to add to the index.
   * @throws IncompleteIdException
   *           If the document or the paragraphs have incomplete or missing identifiers.
   */
  public void add(Document doc) throws IncompleteIdException
  {
    if (doc == null)
      return;
    if (doc.getKey() == null)
    {
      doc.setKey(idpool++);
    }
    else if (doc.getKey() == idpool)
    {
      idpool++;
    }
    documents.add(doc);
    terms.add(doc);
  }

  /**
   * Tests the index to see if it contains the document.
   * 
   * @param key
   *          The document key to test.
   * @return true if the index contains the document else false.
   */
  public boolean containsDocument(Integer key)
  {
    Document d = getDocument(key);
    return d != null;
  }

  /**
   * Removes a document and all of its term mappings from this index.
   * 
   * @param key
   *          the document key to remove
   */
  public void removeDocument(Integer key)
  {
    Iterator<Document> docs = documents.iterator();
    while (docs.hasNext())
      if (docs.next().getKey().equals(key))
        docs.remove();

    Iterator<Paragraph.Id> termmap = terms.keySet().iterator();
    while (termmap.hasNext())
      if (termmap.next().getDocument().equals(key))
        termmap.remove();

  }

  /**
   * Gets the paragraph associated with the a {@link Paragraph.Id}.
   * 
   * @param pid
   *          the {@link Paragraph.Id} associated with this paragraph.
   * @return the Paragraph in the Index with the given id, if it exists, else null.
   */
  public Paragraph getParagraph(Paragraph.Id pid)
  {
    if (pid == null)
      return null;

    Document doc = getDocument(pid.getDocument());
    if (doc.getParagraphs() != null)
      for (Paragraph p : doc.getParagraphs())
      {
        if (p.getId().getOrdinal() == pid.getOrdinal())
          return p;
      }
    return null;
  }

  /**
   * Gets the paragraph frequencies for the given paragraph id.
   * 
   * @param pid
   *          The paragraph id to match.
   * @return The {@link TermMap} that stores the term frequencies for the paragraph.
   */
  public TermMap getParagraphFrequencies(Paragraph.Id pid)
  {
    return terms.get(pid);
  }

  /**
   * @return the {@link #documents} in this Index
   */
  public Iterable<Document> getDocuments()
  {
    return documents;
  }

  /**
   * Gets the number of documents stored in this index.
   * 
   * @return The number of documents stored in this index.
   */
  public int getDocumentCount()
  {
    return documents.size();
  }

  /**
   * @return the {@link TermIndex} for this Index
   */
  public Iterable<Entry<Paragraph.Id, TermMap>> getTerms()
  {
    return terms.entrySet();
  }

  /**
   * Gets a document from this Index using {@link Document#getKey()}.
   * 
   * @param key
   *          The key for the document to retrieve
   * @return the Document with the input key.
   */
  public Document getDocument(Integer key)
  {
    if (key == null)
      return null;
    for (Document d : documents)
    {
      if (key.equals(d.getKey()))
        return d;
    }
    return null;
  }

  /**
   * Installs a TermMapper on this index.
   * 
   * @param mapper
   *          The TermMapper to use when indexing documents.
   */
  public void setTermMapper(TermMapper mapper)
  {
    this.termMapper = mapper;
  }

  /**
   * Stores the collection of {@link Document}s that compose this Index.
   * <p>
   * In the language of an <b>Information Retrieval System</b> this represents the document store.
   * Note that the IRS document is actually represented by the member {@link Paragraph}.
   * 
   * @author James Arlow
   *
   */
  public class Documents extends TreeSet<Document>
  {

    /**
     * Serialization version number; ensures compatibility.
     */
    private static final long serialVersionUID = 1L;

  }

  /**
   * Maps terms to their occurrences across documents.
   * <p>
   * Each document added to the Index has it's paragraphs merged with the TermIndex.
   * <p>
   * In the language of an <b>Information Retrieval System</b>, this represents the IRS index.
   * 
   * @author James Arlow
   *
   */
  public class TermIndex extends TreeMap<Paragraph.Id, TermMap> implements Serializable
  {

    /**
     * Serialization version number; ensures compatibility.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Adds a document to the term index. Each member paragraph has it's key terms merged into the
     * index with a mapping to the paragraph identity.
     * 
     * @param doc
     *          the Document to add to the index.
     * @throws IncompleteIdException
     *           If a paragraph does not have an id.
     */
    void add(Document doc) throws IncompleteIdException
    {
      if (doc.getParagraphs() != null)
      {
        for (Paragraph p : doc.getParagraphs())
        {
          Paragraph.Id id = p.getId();
          if (id == null)
            throw new IncompleteIdException("Paragraph.Id must not be null", p);
          put(id, map(p));
        }
      }
    }

    /**
     * An elvis operation to apply the TermMapper to a paragraph.
     * <p>
     * An elvis operation is one that returns null the variable it is called on is null.
     * 
     * @param p
     *          The paragraph to map the text for.
     * @return The TermMap for the text.
     */
    private TermMap map(Paragraph p)
    {
      String text = p == null ? null : p.getText();
      return termMapper == null || text == null ? null : termMapper.mapTermFrequencies(text);
    }

  }

  /**
   * An exception indicating that an item cannot be added to the index because it's identifying
   * information is null.
   * <p>
   * Generally, this will mean an Integer key is null, such as the document key or paragraph
   * ordinal.
   * 
   * @author James Arlow
   *
   */
  @SuppressWarnings("serial")
  public static class IncompleteIdException extends Exception
  {

    /**
     * Generates the exception with a message that includes the problem field and debug info.
     * 
     * @param field
     *          The field that has missing information
     * @param info
     *          Info describing the state that generated this exception.
     */
    public IncompleteIdException(String field, Object info)
    {
      super("<<" + field + ">> cannot be null: " + info);
    }

  }

}
