package gutensearch.datamodel;

import java.io.Serializable;
import java.util.List;

/**
 * Describes the Gutensearch model for a text document.
 * <p>
 * {@link #key} is a numerical key that acts a unique identifier for the document when stored with
 * others. The Project Gutenberg ebook number would be a good candidate for this field.
 * <p>
 * {@link #title} is the text title of the Project Gutenberg document.
 * <p>
 * {@link #paragraphs} represent the text data of the Project Gutenberg file as an ordered list of
 * {@link Paragraph} objects.
 * <h3>Information Retrieval Systems</h3>
 * <p>
 * In the language of an <i>Information Retrieval System</i>, A GutenSearch document represents a
 * logical container for a sequence of IRS documents. This ambiguity arises because the IRS
 * documents being indexed are, in natural language, ordered paragraphs in a larger text document.
 * GutenSearch adheres to the natural language convention.
 * <p>
 * The GutenSearch model of an IRS document is {@link Paragraph}.
 * 
 * @author James Arlow
 *
 */
public class Document implements Serializable, Comparable<Document>
{

  /**
   * Default Serialization ID.
   * <p>
   * Used to manage serialization compatibility
   */
  private static final long serialVersionUID = 1L;

  /**
   * An integer key that can be used to uniquely identify this document in collections.
   */
  private Integer key;

  /**
   * The ordered collection of Paragraphs that composes the content of the document.
   */
  private List<Paragraph> paragraphs;

  /**
   * The String title of the document, i.e. the book title.
   */
  private String title;

  /**
   * @return {@link #key}
   */
  public Integer getKey()
  {
    return key;
  }

  /**
   * Gets the member paragraph via 0-indexed ordinal.
   * 
   * @param ordinal
   *          The array indexed ordinal of the paragraph.
   * @return The paragraph if it exists.
   */
  public Paragraph getParagraph(Integer ordinal)
  {
    if (paragraphs == null || ordinal == null)
      return null;
    return paragraphs.get(ordinal);
  }

  /**
   * Gets the number of member paragraphs in this document.
   * 
   * @return The number of member paragraphs in this document.
   */
  public int getParagraphCount()
  {
    return paragraphs == null ? 0 : paragraphs.size();
  }

  /**
   * @return the {@link #paragraphs}
   */
  public Iterable<Paragraph> getParagraphs()
  {
    return paragraphs;
  }

  /**
   * @return the {@link #title}
   */
  public String getTitle()
  {
    return title;
  }

  /**
   * Updates the key on this document object, and each member of {@link #paragraphs}.
   * 
   * @param key
   *          the {@link #key} to set
   */
  public void setKey(Integer key)
  {
    this.key = key;
    if (paragraphs != null)
      for (Paragraph p : paragraphs)
        p.getId().setDocument(key);
  }

  /**
   * Updates the {@link Paragraph} collection, and syncs the document keys with {@link #key}.
   * 
   * @param paragraphs
   *          the {@link #paragraphs} to set
   */
  public void setParagraphs(List<Paragraph> paragraphs)
  {
    this.paragraphs = paragraphs;
    if (key != null)
      for (Paragraph p : paragraphs)
        p.getId().setDocument(key);
    int i = 0;
    for (Paragraph p : paragraphs)
      p.getId().setOrdinal(i++);
  }

  /**
   * @param title
   *          the {@link #title} to set
   */
  public void setTitle(String title)
  {
    this.title = title;
  }

  /**
   * Compares to another document based on the document key.
   * 
   * @return A {@link Comparable} int based on the document key.
   */
  @Override
  public int compareTo(Document doc2)
  {
    if (doc2 == null)
      return 1;
    if (key == null)
      return doc2.key == null ? -1 : 0;
    return key.compareTo(doc2.key);
  }

  @Override
  public String toString()
  {
    return "Document[" + key + "]=`" + title + "`";
  }

  /**
   * Adds a paragraph to the document. This has the side effect of updating the paragraph id.
   * 
   * @param p
   *          The paragraph to add to the document.
   */
  public void addParagraph(Paragraph p)
  {
    if (p == null)
      return;
    p.getId().setOrdinal(paragraphs.size());
    paragraphs.add(p);
  }

}
