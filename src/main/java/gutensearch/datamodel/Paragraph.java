package gutensearch.datamodel;

import java.io.Serializable;

/**
 * Describes the gutensearch perspective of a document paragraph.
 * <p>
 * A paragraph is defined logically as continuous section of content text. These content sections
 * are ordered sequentially inside their parent document.
 * <p>
 * {@link Paragraph.Id} acts as an identity, and can be used as a reference / index for access in
 * parent collection.
 * 
 * <h3>Information Retrieval Systems</h3> GutenSearch acts as an <b>Information Retrieval
 * System.</b> It's IRS documents and postings refer to, in natural language, ordered paragraphs
 * inside a published text document (Project Gutenberg Ebooks). Because of the ambiguity these two
 * naming schemes, the datamodel package uses the natural language concepts over the logical ones.
 * <p>
 * {@link Paragraph} represents the IRS notion of a document. The {@link Document} class is a
 * container for an ordered collection of paragraphs.
 * <p>
 * 
 * @author James Arlow
 *
 */
public class Paragraph implements Serializable, Comparable<Paragraph>
{

  /**
   * Default Serialization ID.
   * <p>
   * Used to manage serialization compatibility.
   */
  private static final long serialVersionUID = 1L;

  /**
   * {@link Paragraph#id} represents the identity of the paragraph. It allows a reference to the
   * paragraph to be stored and resolved from a collection of multiple {@link Document}s.
   * <p>
   * In the language of an <b>Information Retrieval System</b>, the id acts as a posting.
   */
  private Id id = new Id();

  /**
   * The text content of the document.
   */
  private String text;

  /**
   * A shorthand descriptor of the {@link Paragraph} which shows it's identity data.
   */
  @Override
  public String toString()
  {
    return "Paragraph(" + id.document + ", " + id.ordinal + ")";
  }

  /**
   * @return the {@link #id}
   */
  public Id getId()
  {
    return id;
  }

  /**
   * @param id
   *          the {@link #id} to set
   */
  public void setId(Id id)
  {
    this.id = id;
  }

  /**
   * @return the {@link text}
   */
  public String getText()
  {
    return text;
  }

  /**
   * @param text
   *          the {@link text} to set
   */
  public void setText(String text)
  {
    this.text = text;
  }

  /**
   * Compares to another paragraph based on its {@link Id}.
   * 
   * @return A {@link Comparable} int based on the paragraph id.
   */
  @Override
  public int compareTo(Paragraph p)
  {
    if (id == null)
      return p.id == null ? 0 : -1;
    return id.compareTo(p.id);
  }

  /**
   * Stores the unique identifiers of a {@link Paragraph}. Can be used to pair the {@link Paragraph}
   * with metadata in other containers.
   * <p>
   * Because this data is a pointer to a location inside a {@link Document} controls the id values
   * at the time of paragraph insertion.
   * <p>
   * In the language of an <b>Information Retrieval System</b>, {@link Paragraph.Id} acts as the
   * link between postings and documents. {@link Paragraph} represents an IRS posting.
   * 
   * @author James Arlow
   *
   */
  public static class Id implements Serializable, Comparable<Paragraph.Id>
  {

    /**
     * Default Serialization ID.
     * <p>
     * Used to manage serialization compatibility.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The {@link Document#key} for the {@link Document} that this {@link Paragraph} is a member of.
     */
    private Integer document;

    /**
     * The ordinal that describes a {@link Paragraph}'s location, relative to other Paragraphs
     * inside the document.
     * <p>
     */
    private Integer ordinal;

    /**
     * @return the {@link #document}
     */
    public Integer getDocument()
    {
      return document;
    }

    /**
     * @param document
     *          the {@link #document} to set
     */
    public void setDocument(Integer document)
    {
      this.document = document;
    }

    /**
     * @return the {@link #ordinal}
     */
    public Integer getOrdinal()
    {
      return ordinal;
    }

    /**
     * @param ordinal
     *          the {@link #ordinal} to set
     */
    public void setOrdinal(Integer ordinal)
    {
      this.ordinal = ordinal;
    }

    /**
     * Eclipse generated hashcode method based on the value of the {@link #document} and
     * {@link #ordinal} properties.
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public int hashCode()
    {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((document == null) ? 0 : document.hashCode());
      result = prime * result + ((ordinal == null) ? 0 : ordinal.hashCode());
      return result;
    }

    /**
     * Eclipse generated equals method based on the value of the {@link #document} and
     * {@link #ordinal} properties.
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      Id other = (Id) obj;
      if (document == null)
      {
        if (other.document != null)
          return false;
      }
      else if (!document.equals(other.document))
        return false;
      if (ordinal == null)
      {
        if (other.ordinal != null)
          return false;
      }
      else if (!ordinal.equals(other.ordinal))
        return false;
      return true;
    }

    /**
     * Compares to another {@link Id} based on the Integer key properties of the Id.
     * 
     * @return A {@link Comparable} int based on the document and ordinal keys.
     */
    @Override
    public int compareTo(Id id2)
    {
      if (id2 == null)
        return 1;

      /*
       * this is kind of screwy, but the checkstyle requirements limit return statements to 4 per
       * method, so the document comparison is aggregated.
       */
      Integer docCompare = null;
      if (document == null)
        docCompare = id2.document == null ? 0 : 1;
      else
        docCompare = document.compareTo(id2.document);
      if (docCompare != 0 || document == null)
        return docCompare;

      if (ordinal == null)
        return (id2.ordinal == null) ? 0 : 1;

      return ordinal.compareTo(id2.ordinal);
    }

  }

}
