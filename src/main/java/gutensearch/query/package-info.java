/**
 * Provides Query and Query result classes that scan the index and compile term match summaries.
 */
package gutensearch.query;
