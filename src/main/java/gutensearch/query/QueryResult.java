package gutensearch.query;

import java.io.Serializable;
import java.util.Scanner;

import gutensearch.datamodel.Paragraph;
import gutensearch.terms.TermMap;

/**
 * A container that aggregates query result data for a given paragraph.
 * 
 * @author James Arlow
 *
 */
public class QueryResult implements Serializable
{

  /**
   * Default serialization id. Used to
   */
  private static final long serialVersionUID = 1L;

  String documentTitle;

  Paragraph paragraph;

  TermMap terms;

  /**
   * Gets the title of the document the paragraph is a member of.
   * 
   * @return The title of the parent document.
   */
  public String getDocumentTitle()
  {
    return documentTitle;
  }

  /**
   * Gets the {@link Paragraph} descriptor for this result. Includes the document and ordinal id
   * information as well as the text content.
   * 
   * @return The {@link Paragraph} for this result.
   */
  public Paragraph getParagraph()
  {
    return paragraph;
  }

  /**
   * Retrieves a mapped term frequency for this results paragraph.
   * 
   * @param term
   *          The term to get the frequency for.
   * @return The frequency of the input term in the paragraph.
   */
  public int getTermFrequency(String term)
  {
    return terms.getFrequency(term);
  }

  /**
   * Get the first line of the paragraph text.
   * 
   * @return The first line of the paragraph text.
   */
  public String firstLine()
  {
    String r = null;
    Scanner s = new Scanner(paragraph.getText());
    if (s.hasNextLine())
      r = s.nextLine();
    s.close();
    return r;
  }

  @Override
  public String toString()
  {
    return "QueryResult(" + documentTitle + "[" + String.valueOf(paragraph.getId().getOrdinal())
        + "], `" + firstLine() + "`)";
  }

}
