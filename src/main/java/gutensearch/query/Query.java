package gutensearch.query;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import gutensearch.datamodel.Index;
import gutensearch.datamodel.Paragraph;
import gutensearch.terms.TermMap;
import gutensearch.terms.TermNormalizer;

/**
 * @author James Arlow
 *
 */
public class Query
{

  transient Index index;

  transient TermNormalizer normalizer;

  /**
   * The terms of the query.
   */
  List<String> terms = new LinkedList<>();

  Set<QueryResult> results;

  /**
   * Builds the QueryResult for the given PID using the index.
   * 
   * <p>
   * NOTE: This message is unsafe if the data is invalid.
   * 
   * @param index
   *          The {@link Index} to retrieve the data from.
   * @param pid
   *          The {@link Id} of the paragraph to build the result from.
   * @return A QueryResult for the Paragraph, based on the data in the index.
   */
  static QueryResult getResultFromIndex(Index index, Paragraph.Id pid)
  {
    QueryResult result = new QueryResult();
    result.paragraph = index.getParagraph(pid);
    result.terms = index.getParagraphFrequencies(pid);
    result.documentTitle = index.getDocument(pid.getDocument()).getTitle();
    return result;
  }

  /**
   * Initiates the execution of the query.
   * 
   * @throws Exception
   *           If the index is null.
   */
  public void execute() throws Exception
  {
    if (index == null)
      throw new NullPointerException("Query#index == null");

    results = generateResultCollection();

    postings: for (Entry<Paragraph.Id, TermMap> e : index.getTerms())
    {
      Paragraph.Id pid = e.getKey();
      for (String term : terms)
      {
        String nterm = normalize(term);
        TermMap map = index.getParagraphFrequencies(pid);
        Integer f = map == null ? 0 : map.getFrequency(nterm);
        if (f == 0)
          continue postings;
      }
      results.add(getResultFromIndex(index, pid));
    }
  }

  /**
   * Gets the results for the current query.
   * 
   * @return Null if {@link #execute()} has not been call, else the QueryResults built by that
   *         method.
   */
  public Set<QueryResult> getResults()
  {
    return results;
  }

  /**
   * Get the list of terms for this query.
   * 
   * @return The list of terms for the query.
   */
  public List<String> getTerms()
  {
    return terms;
  }

  /**
   * Generates a result collection that is ordered based on the term frequencies.
   * 
   * @return An ordered set, whose order is based on the frequency of the query terms in each
   *         result.
   */
  private Set<QueryResult> generateResultCollection()
  {
    Comparator<QueryResult> orderingScheme = new Comparator<QueryResult>()
    {
      @Override
      public int compare(QueryResult o1, QueryResult o2)
      {
        int acc1 = 0;
        int acc2 = 0;
        for (String term : terms)
        {
          String nterm = normalize(term);
          acc1 += o1.getTermFrequency(nterm);
          acc2 += o2.getTermFrequency(nterm);
        }
        return Integer.compare(acc1, acc2) * -1;
      }
    };
    return new TreeSet<>(orderingScheme);
  }

  /**
   * If a normalizer is set, query terms will be normalized before the frequencies are analyzed.
   * 
   * @param term
   *          The input term
   * @return The normalized term, or the input term if nor normalizer is set.
   */
  protected String normalize(String term)
  {
    return normalizer == null ? term : normalizer.normalizeTerm(term);
  }

  /**
   * Sets the index that will be scanned by this query in the execute method.
   * 
   * @param index
   *          The index to scan for query terms.
   */
  public void setIndex(Index index)
  {
    this.index = index;
  }

}
