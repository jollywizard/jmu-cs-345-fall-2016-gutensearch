package gutensearch.storage;

/**
 * Describes a data File in terms of it's type and location.
 * <p>
 * Different types of data files may have different types of access strings.
 * <p>
 * * Local files and resource data may have {@link URI} or {@link Path} location strings.
 * <p>
 * * Project resources may have {@link ClassLoader} resource strings.
 * <p>
 * * Remote files may have {@link URI}/{@link URL} location strings.
 * <p>
 * Abstracting this relationship into away to contain the string and type descriptor allows the
 * resolution facility to be implemented in different ways by different classes.
 * <p>
 * The resolution of {@link FileRef} into access to file content is provided via
 * {@link gutensearch.resolvers.FileResolver}. {@link gutensearch.FileAccess} is the default
 * implementation which resolves each of the {@link FileRef#type} strings defined in
 * {@link FileRef.Types}
 * 
 * @author James Arlow
 *
 */
public class FileRef
{

  /**
   * Identifies the type of the {@link #location} string, indicating how the string is expected to
   * be resolved into data.
   */
  private String type;

  /**
   * A string that can be used to resolve the location of the data with a resolver for the matching
   * {@link #type}.
   */
  private String location;

  /**
   * Defines {@link FileRef#type} strings for the default implementations.
   * 
   * @author James Arlow
   *
   */
  public static final class Types
  {
    /**
     * A URI is a location description String that follows {@link URL} like conventions. A URI type
     * string can be resolved by passing it to {@link URI#create(String)} and then opening it as a
     * {@link URL}.
     * <p>
     * While a URI is not necessarily a URL, it can be resolved into one if it contains the required
     * parts. Resolving a URI to a URL allows its data to be accessed via {@link URL#openStream()}.
     * <p>
     * URIs can represent both local and remote files, as well as relative locations to other
     * concrete URIs.
     */
    public static final String URI_TYPE = "URI";

    /**
     * A file type string is a path string that can be resolved by passing it to
     * {@link java.io.File#File(String).
     */
    public static final String FILE_TYPE = "FILE";
  }

  /**
   * @return the {@link #type}
   */
  public String getType()
  {
    return type;
  }

  /**
   * @param type
   *          the {@link #type} to set
   */
  public void setType(String type)
  {
    this.type = type;
  }

  /**
   * @return the {@link location}
   */
  public String getLocation()
  {
    return location;
  }

  /**
   * @param location
   *          the {@link location} to set
   */
  public void setLocation(String location)
  {
    this.location = location;
  }

}
