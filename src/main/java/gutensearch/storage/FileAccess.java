package gutensearch.storage;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import gutensearch.storage.resolvers.FileResolver;

/**
 * A composite {@link FileResolver} that can be extended with more location types.
 * <p>
 * By default supports {@link FileRef.Types#FILE_TYPE} and {@link FileRef.Types#URI_TYPE}.
 * <p>
 * Supported types can be extended via {@link #register(String, FileResolver)}. Registrations are
 * visible to all {@link FileAccess} instances.
 * <p>
 * {@link FileAccess#openStream(FileRef)} provides static access to this resolve function.
 * 
 * @author James Arlow
 *
 */
public class FileAccess implements FileResolver
{

  /**
   * Default instance for static access.
   */
  static final FileAccess INSTANCE = new FileAccess();

  private static Map<String, FileResolver> mappings = new HashMap<String, FileResolver>();

  /**
   * Creates a composite {@link FileResolver} that supports local path and URI types.
   */
  public FileAccess()
  {
  }

  /**
   * Opens the FileRef using the default behavior specified by {@link FileAccess}.
   * 
   * @param file
   *          the target file descriptor.
   * @return An {@link InputStream} for the target file.
   * @throws Exception
   *           if an I/O error occurs, or the file could not be resolved.
   */
  public static InputStream openStream(FileRef file) throws Exception
  {
    return INSTANCE.resolve(file);
  }

  /**
   * Register a {@link FileResolver} with {@link FileAccess} under the given type key.
   * <p>
   * This registration will be visible to all instances of {@link FileAccess}.
   * 
   * @param key
   *          The key to register the resolver under.
   * @param resolver
   *          The resolver to register.
   */
  public static void register(String key, FileResolver resolver)
  {
    mappings.put(key, resolver);
  }

  @Override
  public InputStream resolve(FileRef doc) throws Exception
  {
    FileResolver resolver = mappings.get(doc.getType());
    if (resolver == null)
      throw new Exception("location type not recognized : " + doc.getType());
    else
      return resolver.resolve(doc);
  }
}
