package gutensearch.storage.index;

import gutensearch.datamodel.Index;

/**
 * Provides an {@link Index} property and provides save and load operations.
 */
public interface IndexStorage
{

  /**
   * Save the Index to the target location.
   * 
   * @throws Exception
   *           If an I/O error occurs.
   */
  public void save() throws Exception;

  /**
   * Load the Index from the target location.
   * <p>
   * Because the index loaded will be a separate object than the current index, the current index
   * will overwritten.
   * 
   * @return The index property that the load operation has replaced, i.e. {@link #getIndex()}
   * @throws Exception
   *           If an I/O error occurs.
   */
  public Index load() throws Exception;

  /**
   * Get the Index if it has been loaded.
   * 
   * @return The current {@link Index}.
   */
  public Index getIndex();

  /**
   * Sets the current {@link Index}.
   * 
   * @param index
   *          The {@link Index} to set.
   */
  public void setIndex(Index index);

}
