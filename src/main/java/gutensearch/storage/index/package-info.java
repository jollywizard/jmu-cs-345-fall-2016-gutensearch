/**
 * Provides saving and loading for {@link gutensearch.datamodel.Document}
 * {@link gutensearch.datamodel.Index} collections.
 * 
 * @author James Arlow
 */
package gutensearch.storage.index;
