package gutensearch.storage.index;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import gutensearch.datamodel.Index;

/**
 * Implements {@link IndexStorage} using a {@link java.io.File} and serialization.
 * 
 * @author James Arlow
 *
 */
public class FileIndexStorage implements IndexStorage
{

  File file;
  Index index;

  /**
   * Create an {@link IndexStorage} whose data is a {@link java.io.File} location.
   * 
   * @param file
   *          The {@link File} location for save/load operations.
   */
  public FileIndexStorage(File file)
  {
    this.file = file;
  }

  @Override
  public void save() throws Exception
  {
    FileOutputStream fos = new FileOutputStream(file);
    ObjectOutputStream os = new ObjectOutputStream(fos);

    try
    {
      os.writeObject(index);
    }
    finally
    {
      os.close();
      fos.close();
    }
  }

  @Override
  public Index load() throws Exception
  {
    FileInputStream fis = new FileInputStream(file);
    ObjectInputStream is = new ObjectInputStream(fis);
    
    try
    {
      Object obj = is.readObject();
      if (Index.class.isInstance(obj))
        index = (Index) obj;
      else
        throw new Exception("Deserialized object is not an Index: " + obj);
      return index;
    }
    finally
    {
      is.close();
      fis.close();
    }
  }

  @Override
  public Index getIndex()
  {
    return index;
  }

  @Override
  public void setIndex(Index index)
  {
    this.index = index;
  }

}
