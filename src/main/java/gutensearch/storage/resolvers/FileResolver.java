package gutensearch.storage.resolvers;

import java.io.InputStream;

import gutensearch.storage.FileRef;

/**
 * Provides the resolution of a {@link FileRef} into an {@link InputStream} for data access.
 * 
 * @author James Arlow
 *
 */
public interface FileResolver
{

  /**
   * Resolve a {@link FileRef} into an {@link InputStream} of file content.
   * 
   * @param ref
   *          The {@link FileRef} whose location is intended to be resolved.
   * @return an {@link InputStream} for the target file data, or null if this resolver could not
   *         resolve the stream.
   * @throws Exception
   *           If the resolver encountered an exception when attempting to access the stream.
   */
  public InputStream resolve(FileRef ref) throws Exception;
}
