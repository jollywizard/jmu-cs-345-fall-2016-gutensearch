package gutensearch.storage.resolvers;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import gutensearch.storage.FileRef;

/**
 * Implements {@link FileResolver} for file path strings. Uses the {@link File#File(String)}
 * facility.
 * 
 * @author James Arlow
 *
 */
public class PathResolver implements FileResolver
{

  @Override
  public InputStream resolve(FileRef doc) throws Exception
  {
    return new FileInputStream(new File(doc.getLocation()));
  }

}
