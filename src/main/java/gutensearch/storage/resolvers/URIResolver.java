package gutensearch.storage.resolvers;

import java.io.InputStream;
import java.net.URI;

import gutensearch.storage.FileRef;

/**
 * Implements {@link FileResolver} for {@link java.net.URI} type strings.
 * <p>
 * Uses {@link java.net.URI#toURL()} to open the target as a {@link java.net.URL} resource.
 * 
 * @author James Arlow
 *
 */
public class URIResolver implements FileResolver
{

  @Override
  public InputStream resolve(FileRef doc) throws Exception
  {
    if (doc.getType() == FileRef.Types.URI_TYPE)
    {
      URI uri = URI.create(doc.getLocation());
      return uri.toURL().openStream();
    }
    return null;
  }

}
