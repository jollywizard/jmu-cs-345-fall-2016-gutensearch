
/**
 * Resolve {@link gutensearch.storage.FileRef} locations to binary data streams. 
 */
package gutensearch.storage.resolvers;
