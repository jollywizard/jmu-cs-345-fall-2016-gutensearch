package gutensearch.query;

import org.junit.Test;

import gutensearch.datamodel.Document;
import gutensearch.datamodel.Index;
import gutensearch.fixtures.datamodel.IndexFixtures;

/**
 * Unit test for the query api.
 * 
 * @author James Arlow
 *
 */
public class QueryTest
{

  /**
   * Basic test for Query.
   * 
   * @throws Exception
   *           if the index has a meltdown.
   */
  @Test
  public void basicQueryTest() throws Exception
  {
    System.out.println("- - -\nsimple test");
    Index i = IndexFixtures.simpleIndex();

    Query q = new Query();
    q.setIndex(i);

    q.getTerms().add("whole");
    q.getTerms().add("lotta");

    q.execute();

    assert (q.getResults().size() > 0);

    System.out.println("query terms: " + q.getTerms());
    System.out.println(q.getResults());
  }

  /**
   * Tests the {@link Query} api on a project gutenberg ebook.
   * 
   * @throws Exception
   *           If an error occurs reading the document or with the index.
   */
  @Test
  public void gutenQueryTest() throws Exception
  {
    System.out.println("- - -\n Ebook Test");
    Index index = IndexFixtures.gutenbergIndex();
    for (Document d : index.getDocuments())
    {
      System.out.println(d);
    }

    Query q = new Query();
    q.setIndex(index);

    String term1 = "be";
    String term2 = "this";

    q.getTerms().add(term1);
    q.getTerms().add(term2);

    q.execute();
    System.out.println("terms: " + q.getTerms());
    for (QueryResult qr : q.getResults())
    {
      System.out.println(qr);
      for (String term : q.getTerms())
      {
        System.out.println("\t" + term + ": " + qr.getTermFrequency(term));
      }
    }
  }

}
