package gutensearch.storage.index;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import gutensearch.datamodel.Document;
import gutensearch.datamodel.Index;
import gutensearch.datamodel.Paragraph;
import gutensearch.fixtures.datamodel.IndexFixtures;

/**
 * Tests the IndexStorage implementations.
 */
public class IndexStorageTest
{

  /**
   * Tests the {@link FileIndexStorage} class using a temporary file and manually constructed Index.
   * 
   * @throws Exception
   *           if an exception occurs during the test (will cause a fail).
   */
  @Test
  public void testFileIndexStorage() throws Exception
  {
    // pad console out for clarity.
    System.out.println("");

    // do the test on a temp file.
    File file = File.createTempFile("guttensearch-IndexStorage-test-", ".java.serialized");
    System.out.println(file);

    try
    {
      Index i = IndexFixtures.simpleIndex();

      final int TEST_KEY = IndexFixtures.SimpleIndexInfo.TEST_KEY;
      Document d = i.getDocument(IndexFixtures.SimpleIndexInfo.TEST_KEY);
      Paragraph p = d.getParagraph(0);

      Assert.assertEquals(i.getDocumentCount(), 1);

      FileIndexStorage fis = new FileIndexStorage(file);
      fis.setIndex(i);

      long lengthBefore = file.length();
      fis.save();
      long lengthAfter = file.length();

      System.out.println("SAVED: " + wrap(i));
      System.out.println("SAVED Document: " + wrap(d));
      System.out.println("SAVED Paragraph: " + wrap(p.getText()));
      System.out.println("SAVED frequencies: " + wrap(i.getParagraphFrequencies(p.getId())));

      // ensure that object was written to file.
      System.out.println("OLD FILE LENGTH" + wrap(lengthBefore));
      System.out.println("NEW FILE LENGTH" + wrap(lengthAfter));
      Assert.assertNotSame(lengthBefore, lengthAfter);

      Index i2 = fis.load();

      System.out.println("- - - ");

      System.out.println("LOADED: " + wrap(i2));
      System.out.println("LOADED DOCUMENT COUNT: " + wrap(i2.getDocumentCount()));

      Document d2 = i2.getDocument(TEST_KEY);
      assert (d2 != null);

      System.out.println("LOADED Document:" + d2);

      // The Indexes are two different objects
      Assert.assertNotSame(i, i2);

      // The Document list is the same size and not empty.
      Assert.assertEquals(i.getDocumentCount(), i2.getDocumentCount());
      Assert.assertEquals(i2.getDocumentCount(), 1);

      Paragraph p2 = i2.getDocument(TEST_KEY).getParagraph(0);
      System.out.println("LOADED Paragraph.text: " + wrap(p2.getText()));
      Assert.assertEquals(IndexFixtures.SimpleIndexInfo.TEST_STRING, p2.getText());

      System.out.println("LOADED Paragraph.Id.document: " + wrap(p2.getId().getDocument()));
      Assert.assertEquals(p2.getId().getDocument(), d.getKey());

      System.out
          .println("LOADED Paragraph frequencies" + wrap(i2.getParagraphFrequencies(p2.getId())));
    }
    finally
    {
      // cleanup temp file
      file.delete();
    }
  }

  /**
   * Wraps an objects tostring in brackets for clarity on the console.
   * 
   * @param in
   *          the object to wrap in brackets
   * @return the toString of the object wrapped in brackets.
   */
  private static String wrap(Object in)
  {
    return "[" + String.valueOf(in) + "]";
  }

}
