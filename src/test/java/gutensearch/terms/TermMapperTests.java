package gutensearch.terms;

import org.junit.Test;

import gutensearch.datamodel.Paragraph;
import gutensearch.fixtures.Fixtures;

/**
 * JUnit test suit for {@link TermMapper} implementations and use cases.
 * 
 * @author James Arlow
 *
 */
public class TermMapperTests
{

  /**
   * Tests the {@link DefaultTermMapper}.
   * 
   * @throws Exception
   *           If an i/o error occurs during testing.
   */
  @Test
  public void defaultTermMapperTest() throws Exception
  {
    DefaultTermMapper mapper = new DefaultTermMapper();

    Paragraph p = new Paragraph();
    p.setText(Fixtures.read("frog-song.txt"));

    TermMap map = mapper.mapTermFrequencies(p.getText());

    System.out.println(p.getText());
    System.out.println(map);

    // verify that interior punctuation works.
    assert (map.contains("heart's"));
    assert (map.contains("rag-time"));
  }

}
