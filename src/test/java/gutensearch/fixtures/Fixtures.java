package gutensearch.fixtures;

import java.io.InputStream;
import java.util.Scanner;

import gutensearch.storage.FileAccess;
import gutensearch.storage.FileRef;
import gutensearch.storage.resolvers.FileResolver;

/**
 * Defines a FileResolver and location for resource data in this package.
 * <p>
 * Registers itself with the global {@link FileResolver}, {@link FileAccess}, on load.
 * 
 * @author James Arlow
 *
 */
public class Fixtures
{

  /**
   * The {@link FileRef#getType()} for fixture data.
   */
  private static final String REF_TYPE = "FIXTURES";

  /**
   * Static Initializer: <br/>
   * When this class is opened by the JVM for the first time, it will be registered with
   * {@link FileAccess}.
   * <p>
   * This ensures that it is only registered once, and that a resolver is only registered when
   * Fixtures is active in the program.
   */
  static
  {
    System.out.println("REGISTERING FIXTURE RESOLVER: " + Fixtures.class.getName());
    FileAccess.register(REF_TYPE, Resolver.INSTANCE);
  }

  /**
   * Get a {@link FileRef} for a java {@link ClassLoader} resource in the package or it's
   * subpackages.
   * <p>
   * {@link FileResolver} uses the class {@link Fixtures}, to deduce the base location, and then the
   * path is resolved like a normal file system path, whether the code is unpacked in the system, or
   * packed in an archive.
   * 
   * @param location
   *          The path to the resource {@link gutensearch.fixtures} is the base path. This must be a
   *          '\' file system like path, not a '.' delimited java path.
   * @return A FileRef to the fixture location.
   */
  public static FileRef getRef(String location)
  {
    FileRef fr = new FileRef();
    fr.setLocation(location);
    fr.setType(REF_TYPE);
    return fr;
  }

  /**
   * Opens the fixture as an {@link InputStream}.
   * 
   * @param id
   *          The fixture file name / fixture relative path.
   * @return An {@link InputStream} for reading the fixture contents.
   * @throws Exception
   *           if an I/O error occurs, such as an invalid id.
   */
  public static InputStream open(String id) throws Exception
  {
    return FileAccess.openStream(getRef(id));
  }

  /**
   * Gets the fixture file and reads it into a string.
   * 
   * @param id
   *          The fixture id/path.
   * @return The contents of the fixture as a string.
   * @throws Exception
   *           if an I/O error occurs, such as an invalid id.
   */
  public static String read(String id) throws Exception
  {
    StringBuilder sb = new StringBuilder();

    Scanner in = new Scanner(open(id));
    while (in.hasNextLine())
    {
      sb.append(in.nextLine());
      sb.append("\n");
    }
    in.close();
    return sb.toString();
  }

  /**
   * Implements {@link FileResolver} using the {@link Class#getResourceAsStream(String)} on
   * {@link Fixtures}.
   * <p>
   * Opens files stored in the <code>gutensearch.fixtures</code> package or it's subpackages.
   * 
   * @author James Arlow
   *
   */
  public static class Resolver implements FileResolver
  {
    /**
     * Singleton Instance.
     */
    static final Resolver INSTANCE = new Resolver();

    @Override
    public InputStream resolve(FileRef doc) throws Exception
    {
      return Fixtures.class.getResourceAsStream(doc.getLocation());
    }

  }

}
