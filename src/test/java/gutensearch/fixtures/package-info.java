/**
 * Stores data files and providers for testing.
 * 
 * Referred to as fixtures because they are not dynamic system resources, but static, predictable
 * data that can be included for testing.
 * 
 * @author James Arlow
 */
package gutensearch.fixtures;
