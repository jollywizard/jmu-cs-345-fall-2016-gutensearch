/**
 * Suppliers of fixture driven datamodel objects.
 * 
 * @author James Arlow
 *
 */
package gutensearch.fixtures.datamodel;
