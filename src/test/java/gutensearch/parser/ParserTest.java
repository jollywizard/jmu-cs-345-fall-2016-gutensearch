package gutensearch.parser;

import org.junit.Test;

import gutensearch.datamodel.Paragraph;
import gutensearch.fixtures.Fixtures;
import gutensearch.storage.FileRef;

/**
 * Test suite for {@link Parser}.
 * 
 * @author James arlow
 *
 */
public class ParserTest
{

  /**
   * Tests the creation of a Parser using the fixture file pg34175.txt
   * <p>
   * Generates text on the console out, since it is also used when prototyping the data model.
   * 
   * @throws Exception
   *           If an error occurs during testing.
   */
  @Test
  public void test() throws Exception
  {
    FileRef ref = Fixtures.getRef("pg34175.txt");

    Parser p = new Parser(ref);
    p.processTarget();

    if (p.document != null)
    {
      System.out.println(p.document.getTitle());
      System.out.println(p.document.getParagraphCount());

      final int JUST_10 = 10;
      for (int i = 0; i < JUST_10; i++)
      {
        print(p.document.getParagraph(i));
      }
      print(p.document.getParagraph(p.paragraphs.size() - 1));
    }
  }

  /**
   * Displays a paragraph to the console for verification of parser result details.
   * 
   * @param p
   *          The paragraph to display.
   */
  private static void print(Paragraph p)
  {
    System.out.print("[" + p.getId().getOrdinal() + "] {");
    System.out.print(p.getText());
    System.out.println("}");
  }
}
