/**
 * This package stores code that demonstrates concepts in action.
 * <p>
 * It is not intended to be part of the deliverables, but may serve as the basis for code reuse
 * during implentation.
 * <p>
 * As the code base changes, proof of concepts that are no longer compatible will be removed, so
 * this package will often be empty.
 * 
 * @author James Arlow
 *
 */
package gutensearch.proofofconcept;
